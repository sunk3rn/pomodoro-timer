# <b>⏲️Pomodoro timer</b>
 *Vytvořil Karel Kobliha* 

Technika Pomodoro je metoda organizace času. Tradičně se používá například jako metoda k učení.


⏳ Provádí v šesti krocích:

<ol>
    <li>Rozhodněte se, jaký úkol chcete splnit.
    <li>Nastavte časovač (tradičně se používá 25 minut).
    <li>Dokud časovač nezazvoní, pracujte na úkolu. Pokud vás během práce vyruší myšlenka, zapište si ji na kus papíru, ale hned se zase vraťte zpět k práci.
    <li>Až časovač zazvoní, udělejte si na papír značku.
    <li>Pokud máte na papíru méně než 4 značky, dejte si na chvíli (3-5 minut) pauzu, pak se vraťte zpět ke kroku číslo 1.
    <li>Po čtyřech takovýchto pomodoro cyklech si dejte větší pauzu (asi 15-30 minut), vynulujte si počet značek na papíře a pokračujte krokem číslo 1.
</ol>

----
Tento časovač za vás řesí právě kroky 4, 5 a 6 + časování. To dokáže pomocí vývojové desky Nucleo STM8S208RB, Maticového displeje ovládaného pomocí čipu MAX7219, čtyřmístného displeje poháněného čipem TM1637, aktivním bzučákem a 4 barevnými tlačítky. Jen ten papír si budete bohužel muset sehnat sami.

# 🔎 Funkce
Momentálně lze nastavit doba hlavního časovače, doba malé a velké přestávky. Pro jednoduchost ovládání (na úkor lehké programovatelnosti) se tento pomodoro časovač ovládá pouze 4 tlačítky.  Hlavní časovač se spouští čeveným tlačítkem a také se jím dá zastavit. V zastaveném stavu lze přidávat či odečítat čas z již jmenovaných 3 časů, zelené tlačítko zvětšuje a modré zmenšuje, obě operace po 10s. Žluté pak přepíná mezi jednotlivými režimy.


<br>

# ⚙ Součástky
| Název    | Popis                                         | Odkaz                                                                                                           | Cena   | Cena celkem       |
| -------- | --------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | ------ | ----------------- |
| STM8S    | MCU                                           | [fernell](https://cz.farnell.com/stmicroelectronics/nucleo-8s208rb/nucleo-64-development-board-8bit/dp/2980962) | 288 Kč | 288 Kč            |
| MAX7219  | 8x8 maticový displej s budičem MAX7219        | [laskakit](https://www.laskakit.cz/8x8-led-matice-s-max7219-3mm-cervena/)                                       | 68 Kč  | 68 Kč             |
| TM1637   | Sedmisegmentový displej s budičem TM1637      | [laskakit](https://www.laskakit.cz/hodinovy-displej-tm1637--cerveny/)                                           | 34 Kč  | 34 Kč             |
| Tlačítka | Tlačítka Červené, Modré, Zelené a žluté bravy | [laskakit](https://www.laskakit.cz/mikrospinac-tc-1212t-12x12x7-3mm/)                                           | 4 Kč   | 16 Kč             |
| Bzučák   | Aktivní bzučák                                | [laskakit](https://www.laskakit.cz/aktivni-bzucak-3-3v/)                                                        | 6 kč   | 6 Kč              |
|          | Cena celkem                                   |                                                                                                                 |        | 124 Kč (+288 Kč ) |

## Sedmisegmentový displej (TM1637)

Pro zobrazení času je používán červený čtyřmístný sedmisegmentový dislej s ':' uprosřed. Ovládaný je pomocí čipu TM1637. Ten s STM8 komunikuje pomocí nestardatizovaného rozhraní přes 2 konektory CLK (Clock) a DIN (Vstup dat). Přes toto rozhraní(Na pinu DIN) je do displeje posílán řetězec ve tvaru "00:00" nebo "00 00" podle toho, jestli chceme rozsvítit dvojtečku.

## 8x8 LED Matice (MAX7219)
Pro lepší orientaci v ovládání a pro zajímavější grafickou odezvu jsem přidal 8x8 matici/displej. Ovládá jej budič MAX7219. Tento displej má 5 pinů: 2 napájecí, DIN, CLK, CS. Budič obsahuje 16bitový posuvný registr. CLK určuje hodinový takt, DIN příjmá data a CS určuje začátek a konec zprávy pro budič. Do budiče jsou posílány data na adresy řádků (1-8), následně v binárním tvaru hodnoty osmi bitů (1- bod displeje je zapnutý, 0- je vypnutý). Příkaz může tedy vypadat např. takto:

Tento příkaz rozsvítí 3. LED na prvním řádku:
```
max7219_posli(1,0b00100000);
```

# 🔄 Vývojový diagram

```mermaid
flowchart TB
    Pwr[START]-->Start[Inicializace]
    Start[Inicializace] -->INP[Čeká na vstup]--> BTN{Bylo zmáčknuto tlačítko?}
    BTN{Bylo zmáčknuto tlačítko?} --Červené--> 1[Spustí se časovač]-->1.1[Odečte se 1 s]-->BTN2{Bylo zmáčknuto červené tlačítko?}--Ne-->1.1
    BTN2--Ano-->1.C[Zastaví se časovač]-->1.D{Bylo zmáčknuto červné tlačítko}--Ano-->1
    1.D--Ne-->1.C
    BTN2--Ne-->Bzz{Doběhl čas režimu?}--Ano-->1.A[Zabzučí bzučák]-->1.3{Je 3. režim?}--Ano--> Res[Reset časů, Pauz]-->INP
    1.3--Ne-->1.4{Je 2. Režim?}--Ano-->1.5[Počet pauz +=1]-->1.B[Režim = 1]-->1
    1.4--Ne-->1.6[Je 1. Režim]-->1.7{Je počet pauz 3?}--Ano-->1.8[Režim = 3]-->1
    1.7--Ne-->1.E[Režim = 2 ]-->1
    
    Bzz{Doběhl čas režimu?}--Ne-->1.1
    BTN{Bylo zmáčknuto tlačítko?} --Modré--> 2[Odečte se čas režimu]-->INP
    BTN{Bylo zmáčknuto tlačítko?} --Zelené-->3[Přičte se čas režimu]-->INP
    BTN{Bylo zmáčknuto tlačítko?} --Žluté-->chk{Je třetí režim ?}--Ne-->rez[Režim +=1]--> INP
    chk --Ano--> ch[Režim = 1]-->INP
```

# ⬜ Blokové schéma

```mermaid
flowchart TB
    USB[PC]--+5V-->MCU[STM8]
    MCU--+5V-->MAX7219[8x8 Matice]
    MCU--Data-->MAX7219[8x8 Matice]
    MCU--+5V-->TM1637[Sedmisegment]
    MCU--Data-->TM1637[Sedmisegment]
    BTNS[Tlačítka R,G,B,Y]-->MCU--->BUZ[Bzučák]
```

<br>

# 🗺️ Schéma zapojení

![Schéma zapojení z kiCADu](pomodoro-MCU.svg "Schéma zapojení")


<br>

# 📷 Fotky zařízení

![Fotka1](pomodoro1.jpg "Statická fotka")

![Fotka2](pomodoro2.jpg "Fotka za běhu zařízení")
Fotka za běhu zařízení

### [Video zařízení v chodu](https://youtu.be/ggn6c9yElC8)



# 🏁 Závěr
Vytváření tohoto zařízení bylo zajímavé. Nejvíce času jsem strávil na vymýšlení logiky časovače a na HW zapojení. Celkově si myslím, že mě projekt hodně naučil včetně Markdownu, Git systému a nějaké ty funkce v Cčku.


# 📚 Použité knihovny:

* [MAX 7219](http://www.elektromys.eu/clanky/stm8_8_max7219/clanek.html) - Elektromyš
* [TM1637](https://github.com/indigo6alpha/stm8s-tm1637) - github/indigo6alpha




