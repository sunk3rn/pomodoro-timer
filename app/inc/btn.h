#ifndef INC_BTN_H
#define INC_BTN_H

#include "stm8s.h"

uint8_t tlacitko_je_zmacknute_R(void);
uint8_t tlacitko_je_zmacknute_G(void);
uint8_t tlacitko_je_zmacknute_B(void);
uint8_t tlacitko_je_zmacknute_Y(void);

void tlacitka_init(void);

void beep(unsigned long ms);

#endif