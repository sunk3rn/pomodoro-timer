#include "stm8s.h"
#include "MAX8X8.h"
#include "gpio.h"
#include "clock.h"
#include "tm1637.h"
#include "btn.h"

void main(void)
{
	//Inicializované hodnoty časů, měněné pomocí tlačítek
	int min1 = 10, secs1 = 0;
	int min2 = 0, secs2 = 20;
	int min3 = 0, secs3 = 30;

	//Hodnoty času se kterými časovač se počítá
	int tmp_min1 = min1, tmp_secs1 = secs1;
	int tmp_min2 = min2, tmp_secs2 = secs2;
	int tmp_min3 = min3, tmp_secs3 = secs3;

	char szTemp[8]; //Proměná, která posílá Znaky do sedmisegmentu
	int rezim = 1; //Určuje okamžitý režim zařízení
	int pocet_pauz = 0; //určuje počáteční počet pauz 
	int prvni_zapnuti = 0;
	int komp = 0; //jednoduchá kompenzace odklonu času, kalibrováno podle 10 minutového měření, po 10 min. odklon 3s

	tlacitka_init();

	CLK_Init(CLK_SRC_HSI, CLK_HSI_DIV_NONE, CLK_CPU_DIV_MASTER_NONE); //Inicializace časovače

	tm1637Init(GPIO_PORT_E, GPIO_PIN_5, GPIO_PORT_D, GPIO_PIN_3); //Incializace a nastavení displeje TM1637
	tm1637SetBrightness(1); //Nastavení jasu TM1637

	max7219_init(); //Incializace displeje MAX7219

	while (1)
	{

		switch (rezim)
		{
		case 1: //Posílá inicializační časy do displeje
			if (prvni_zapnuti == 0)
			{
				sprintf(szTemp, "%02d %02d", min1, secs1);
			}
			else
			{
				sprintf(szTemp, "%02d:%02d", tmp_min1, tmp_secs1);
			}
			break;
		case 2:
			if (prvni_zapnuti == 0)
			{
				sprintf(szTemp, "%02d %02d", min2, secs2);
			}
			else
			{
				sprintf(szTemp, "%02d:%02d", tmp_min2, tmp_secs2);
			}
			break;

		case 3:
			if (prvni_zapnuti == 0)
			{
				sprintf(szTemp, "%02d %02d", min3, secs3);
			}
			else
			{
				sprintf(szTemp, "%02d:%02d", tmp_min3, tmp_secs3);
			}
			break;

		default:
			sprintf(szTemp, "%02d:%02d", min1, secs1);
			break;
		}

		tm1637ShowDigits(szTemp);

		if (tlacitko_je_zmacknute_Y()) //Čeká na zmáčknutí Žlutého tlačítka, po stisknutí změní režim
		{
			max_kruhac();

			rezim += 1;
			if (rezim == 4)
			{
				rezim = 1;
			}
			CLK_Delay_ms(500);
		}

		if (tlacitko_je_zmacknute_G()) //Čeká na zmáčknutí Zeleného tlačítka, po stisknutí přidá 10s danému času režimu
		{
			max_plus();

			switch (rezim) // Řeší přidávání minut po překročení 60s
			{
			case 1:
				secs1 += 10;
				if (secs1 == 60)
				{
					secs1 = 00;
					min1++;
				}
				CLK_Delay_ms(200);
				break;

			case 2:
				secs2 += 10;
				if (secs2 == 60)
				{
					secs2 = 00;
					min2++;
				}
				CLK_Delay_ms(200);
				break;

			case 3:
				secs3 += 10;
				if (secs3 == 60)
				{
					secs3 = 00;
					min3++;
				}
				CLK_Delay_ms(200);
				break;

			default:
				break;
			}
		}

		if (tlacitko_je_zmacknute_B()) //Čeká na zmáčknutí Modrého tlačítka, po stisknutí odečte 10s danému času režimu
		{
			max_minus();

			switch (rezim) // Řeší odčítájní minut po překročení 0s
			{
			case 1:
				secs1 -= 10;
				if (secs1 < 0)
				{
					secs1 = 50;
					min1--;
				}
				CLK_Delay_ms(200);
				break;

			case 2:
				secs2 -= 10;
				if (secs2 < 0)
				{
					secs2 = 50;
					min2--;
				}
				CLK_Delay_ms(200);
				break;

			case 3:
				secs3 -= 10;
				if (secs3 < 0)
				{
					secs3 = 50;
					min3--;
				}
				CLK_Delay_ms(200);
				break;

			default:
				break;
			}
		}

		if (tlacitko_je_zmacknute_R()) //Čeká na zmáčknutí Červeného tlačítka, po stisknutí spusti pomodoro cyklus
		{
			CLK_Delay_ms(500);
			max_work();

			if (prvni_zapnuti == 0)
			{
				rezim = 1;
				tmp_min1 = min1;
				tmp_secs1 = secs1;
			}

			while (1)
			{
				prvni_zapnuti = 1;

				tm1637ShowDigits(szTemp);

				switch (rezim) // Řeší odečítání minut po vypršení sekund
				{
				case 1:
					if (tmp_secs1 == 00)
					{
						tmp_secs1 = 59;
						tmp_min1--;
					}

					break;

				case 2:
					if (tmp_secs2 == 00)
					{
						tmp_secs2 = 59;
						tmp_min2--;
					}
					break;

				case 3:
					if (tmp_secs3 == 00)
					{
						tmp_secs3 = 59;
						tmp_min3--;
					}
					break;

				default:
					break;
				}
				tm1637ShowDigits(szTemp);

				CLK_Delay_ms(1150);

				switch (rezim) //Tento switch odečítá sekundy a zobrazuje čas současného režimu
				{
				case 1:
					sprintf(szTemp, "%02d:%02d", tmp_min1, tmp_secs1);

					tmp_secs1--;
					komp +=1;
					if (komp == 20)
					{
						CLK_Delay_ms(500);
						komp = 0;
					}
					break;

				case 2:
					sprintf(szTemp, "%02d:%02d", tmp_min2, tmp_secs2);

					tmp_secs2--;
					komp +=1;
					if (komp == 20)
					{
						CLK_Delay_ms(500);
						komp = 0;
					}
					break;

				case 3:
					sprintf(szTemp, "%02d:%02d", tmp_min3, tmp_secs3);

					komp +=1;
					if (komp == 20)
					{
						CLK_Delay_ms(500);
						komp = 0;
					}
					break;

				default:
					break;
				}
				if ((tmp_min1 == 0) && (tmp_secs1 == 0)) //Kontroluje, jestli skončil čas režimu 1
				{
					tmp_min1 = min1;
					tmp_secs1 = secs1;

					max_pause();

					beep(100);

					if (pocet_pauz == 3)
					{
						rezim = 3;
					}

					if (pocet_pauz != 3)
					{
						rezim = 2;
						pocet_pauz += 1;
					}

					komp = 0;
				}

				if ((tmp_min2 == 0) && (tmp_secs2 == 0)) //Kontroluje, jestli skončil čas režimu 2
				{
					max_work();

					tmp_min2 = min2;
					tmp_secs2 = secs2;

					beep(100);

					rezim = 1;
					komp = 0;
				}

				if ((tmp_min3 == 0) && (tmp_secs3 == 0))  //Kontroluje, jestli skončil čas režimu 3
				{
					max_kostka();

					beep(100);

					tmp_min3 = min3;
					tmp_secs3 = secs3;

					pocet_pauz = 0;
					rezim = 1;
					komp = 0;

					break;
				}

				if (tlacitko_je_zmacknute_R()) //Kód pro zastavení časovače
				{
					max_krizek();
					CLK_Delay_ms(1000);
					break;
				}
			}
		}
	}
}
