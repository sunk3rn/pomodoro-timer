#include "btn.h"
#include "clock.h"

void tlacitka_init(void)
{
    GPIO_Init(GPIOC, GPIO_PIN_1, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(GPIOC, GPIO_PIN_2, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_FAST);
}

uint8_t tlacitko_je_zmacknute_G(void)
{
    return (GPIO_ReadInputPin(GPIOC, GPIO_PIN_1) == 0);
}

uint8_t tlacitko_je_zmacknute_R(void)
{
    return (GPIO_ReadInputPin(GPIOC, GPIO_PIN_4) == 0);
}

uint8_t tlacitko_je_zmacknute_B(void)
{
    return (GPIO_ReadInputPin(GPIOC, GPIO_PIN_2) == 0);
}

uint8_t tlacitko_je_zmacknute_Y(void)
{
    return (GPIO_ReadInputPin(GPIOC, GPIO_PIN_3) == 0);
}

void beep (unsigned long ms) //funkce na pípání bzučákem
{
    GPIO_WriteReverse(GPIOC, GPIO_PIN_6);
    CLK_Delay_ms(ms);
    GPIO_WriteReverse(GPIOC, GPIO_PIN_6);
}